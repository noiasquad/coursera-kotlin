fun main(args: Array<String>) {
    val words = "one two three one one two three three three three".split(' ')
    val frequentWords = words.groupingBy { it }.eachCount().filterValues { it > 2 }.keys
    println(frequentWords)
}