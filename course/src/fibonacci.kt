/**
 * implement the function that builds a sequence of Fibonacci numbers
 * using 'sequence' function ('buildSequence' is its old name).
 */

fun fibonacci(): Sequence<Int> = sequence {
    var lastPair = Pair(0, 1)
    while (true) {
        yield(lastPair.first)
        lastPair = Pair(lastPair.second, lastPair.first + lastPair.second)
    }
}

fun main(args: Array<String>) {
    fibonacci().take(4).toList().toString() eq
            "[0, 1, 1, 2]"

    fibonacci().take(10).toList().toString() eq
            "[0, 1, 1, 2, 3, 5, 8, 13, 21, 34]"
}