/*
A valid identifier is a non-empty string that starts with a letter or underscore and consists of only letters, digits and underscores.
 */

fun hasOnlyLettersOrDigits(s: String): Boolean = when {
    s.isEmpty() -> true
    s[0] == '_' -> hasOnlyLettersOrDigits(s.drop(1))
    s[0].isLetterOrDigit() -> hasOnlyLettersOrDigits(s.drop(1))
    else -> false
}

fun isValidIdentifier(s: String): Boolean {
    return if (!s.isEmpty()) {
        when {
            s[0] == '_' -> hasOnlyLettersOrDigits(s.drop(1))
            s[0].isLetter() -> hasOnlyLettersOrDigits(s.drop(1))
            else -> false
        }
    } else false
}

fun main(args: Array<String>) {
    println(isValidIdentifier("name"))   // true
    println(isValidIdentifier("Name"))   // true
    println(isValidIdentifier("_name"))  // true
    println(isValidIdentifier("_Name"))  // true
    println(isValidIdentifier("_N12"))  // true
    println(isValidIdentifier("_12"))    // true
    println(isValidIdentifier(""))       // false
    println(isValidIdentifier("012A"))    // false
    println(isValidIdentifier("no$"))    // false
}