/**
 * Functions 'all', 'none' and 'any' are interchangeable, you can use one or the other to implement the same functionality.
Implement the functions 'allNonZero' and 'containsZero' using all three predicates in turn. 'allNonZero' checks that all
the elements in the list are non-zero; 'containsZero' checks that the list contains zero element. Add negation (before 'any',
'all' or 'none') where necessary.
 */

fun main(args: Array<String>) {
    val list1 = listOf(1, 2, 3)
    list1.allNonZero() eq true
    list1.allNonZero1() eq true
    list1.allNonZero2() eq true

    list1.containsZero() eq false
    list1.containsZero1() eq false
    list1.containsZero2() eq false

    val list2 = listOf(0, 1, 2)
    list2.allNonZero() eq false
    list2.allNonZero1() eq false
    list2.allNonZero2() eq false

    list2.containsZero() eq true
    list2.containsZero1() eq true
    list2.containsZero2() eq true
}

fun List<Int>.allNonZero(): Boolean = all { it != 0}
fun List<Int>.allNonZero1(): Boolean = none { it == 0}
fun List<Int>.allNonZero2(): Boolean = !any { it == 0}
fun List<Int>.containsZero(): Boolean = !all { it != 0}
fun List<Int>.containsZero1(): Boolean = !none { it == 0}
fun List<Int>.containsZero2(): Boolean = any { it == 0}