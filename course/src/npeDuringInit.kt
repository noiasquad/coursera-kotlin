/**
 * Complete the declaration of the class A so that NPE was thrown during the creation of its subclass B instance.
 */

open class AA(open val value: String) {
    init {
        value.length
    }
}

class B(override val value: String) : AA(value)

open class AAA(val value: String) {
    init {
        value.length
    }
}

class BB(value: String) : AAA(value)


fun main(args: Array<String>) {
    println("BB...")
    BB("a")
    println("B...")
    B("a")
}