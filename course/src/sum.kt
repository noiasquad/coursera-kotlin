fun List<Int>.sum1(): Int {
    var result = 0
    for (i in this) {
        result += i
    }
    return result
}

fun List<Int>.sum(): Int = fold(initial = 0, operation = { acc, x -> acc + x})

fun main(args: Array<String>) {
    println(listOf(1, 2, 3).sum())    // 6
    println(listOf(1, 2, 3).sum1())    // 6
}