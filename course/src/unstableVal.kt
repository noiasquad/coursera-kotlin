import kotlin.random.Random

/**
 * Implement the property 'foo' so that it produces a different value on each access.
 */

var foo2: Int = 0
val foo: Int
    get() {
        return foo2++
    }
val foo3: Int
    get() {
        return Random.nextInt()
    }

fun main(args: Array<String>) {
    // The values should be different:
    println(foo)
    println(foo)
    println(foo)
    // The values should be different:
    println(foo3)
    println(foo3)
}