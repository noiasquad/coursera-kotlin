package mastermind

data class Evaluation(val rightPosition: Int, val wrongPosition: Int)

fun evaluateGuess(secret: String, guess: String): Evaluation {

    fun String.removeFirst(ch: Char): String = replaceFirst("$ch", "")
    fun String.dropFirst(): String = drop(1)

    fun removeOneToOneMatches(a: String, b: String, diff_a: String = "", diff_b: String = ""): Pair<String, String> {
        return when {
            a.isEmpty() -> Pair(diff_a, diff_b)
            a[0] != b[0] -> removeOneToOneMatches(a.dropFirst(), b.dropFirst(), diff_a + a[0], diff_b + b[0])
            else -> removeOneToOneMatches(a.dropFirst(), b.dropFirst(), diff_a, diff_b)
        }
    }

    fun removeAnyMatches(a: String, b: String): String {
        return if (b.isEmpty()) a
        else removeAnyMatches(a.removeFirst(b[0]), b.dropFirst())
    }

    val phase1 = removeOneToOneMatches(secret, guess)
    val phase2 = removeAnyMatches(phase1.first, phase1.second)

    return Evaluation(secret.length - phase1.first.length, phase1.first.length - phase2.length)
}
