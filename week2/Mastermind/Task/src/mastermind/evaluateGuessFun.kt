package mastermind

data class EvaluationFun(val positions: Int, val letters: Int)

infix fun <T> T.eq(other: T) {
    if (this == other) println("OK")
    else println("Error: $this != $other")
}

fun evaluateGuessFun(secret: String, guess: String): EvaluationFun {

    val positions = secret.zip(guess).count { pair -> pair.first == pair.second }

    val commonLetters = "ABCDEF".sumBy { ch ->
        Math.min(secret.count { s -> s == ch }, guess.count { g -> g == ch })
    }

    return EvaluationFun(positions, commonLetters - positions)
}

fun main(args: Array<String>) {
    val result = EvaluationFun(positions = 1, letters = 1)
    evaluateGuessFun("BCDF", "ACEB") eq result
    evaluateGuessFun("AAAF", "ABCA") eq result
    evaluateGuessFun("ABCA", "AAAF") eq result
}
