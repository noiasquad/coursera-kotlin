package nicestring

val ALPHABET = 'a'..'z'
val UNWANTED = listOf("bu", "ba", "be")
val VOWELS = listOf('a', 'e', 'i', 'o', 'u')

private fun Char.isVowel(): Boolean = VOWELS.any { it == this }
private fun Char.duplicate(): String = "$this$this"

fun String.isNice(): Boolean {
    return listOf(
            UNWANTED.none { unwanted -> unwanted in this },
            (count { it -> it.isVowel() } >= 3),
            ALPHABET.map { alpha -> alpha.duplicate() }
                    .any { pair -> pair in this }
    ).count({it}) >= 2
}