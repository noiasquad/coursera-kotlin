package taxipark

/*
 * Task #1. Find all the drivers who performed no trips.
 */
fun TaxiPark.findFakeDrivers(): Set<Driver> =
        this.allDrivers.minus(this.trips.groupBy { it.driver }.keys)

/*
 * Task #2. Find all the clients who completed at least the given number of trips.
 */
fun TaxiPark.findFaithfulPassengers(minTrips: Int): Set<Passenger> =
        if (minTrips > 0) {
            this.trips.flatMap { trip -> trip.passengers.map { passenger -> passenger to trip } }
                    .groupBy { it.first }
                    .filterValues { trips -> trips.size >= minTrips }
                    .keys
        } else allPassengers

/*
 * Task #3. Find all the passengers, who were taken by a given driver more than once.
 */
fun TaxiPark.findFrequentPassengers(driver: Driver): Set<Passenger> =
        this.trips.flatMap { trip -> trip.passengers.map { passenger -> passenger to trip.driver } }
                .filter { it.second == driver}
                .groupBy { it.first }
                .mapValues { entry -> entry.value.groupingBy { it }.eachCount().filterValues { it > 1 }.keys }
                .filterValues { frequentDrivers -> frequentDrivers.isNotEmpty() }
                .keys

/*
 * Task #4. Find the passengers who had a discount for majority of their trips.
 */
fun TaxiPark.findSmartPassengers(): Set<Passenger> =
        this.trips.flatMap { trip -> trip.passengers.map { passenger -> passenger to trip } }
                .groupBy { it.first }
                .mapValues { trip -> trip.value.map { it.second.discount ?: 0.0 } }
                .mapValues { discount -> discount.value.partition { it > 0.0 } }
                .filterValues { it -> it.first.size > it.second.size }
                .keys

/*
 * Task #5. Find the most frequent trip duration among minute periods 0..9, 10..19, 20..29, and so on.
 * Return any period if many are the most frequent, return `null` if there're no trips.
 */
fun TaxiPark.findTheMostFrequentTripDurationPeriod(): IntRange? {
    fun period(duration: Int): IntRange = (duration / 10) * 10..(duration / 10) * 10 + 9

    return this.trips.map { period(it.duration) }
            .groupingBy { it }.eachCount()
            .maxBy { it.value }
            ?.key
}

/*
 * Task #6.
 * Check whether 20% of the drivers contribute 80% of the income.
 */
fun TaxiPark.checkParetoPrinciple(): Boolean {
    val incomes = this.trips.groupBy { it.driver }
            .mapValues { entry -> entry.value.map { it.cost }.sum() }
            .values
            .sortedDescending()

    return if (incomes.isNotEmpty()) {
        val topIncomes = incomes.take(this.allDrivers.size / 5)
        val totalIncome = incomes.sum()
        topIncomes.sum() >= totalIncome * 0.8
    } else false
}