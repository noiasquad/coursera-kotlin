package board

open class SquareBoardImpl(override val width: Int) : SquareBoard {

    protected open class EmptyCell(override val i: Int, override val j: Int) : Cell

    protected open val board: Array<Cell> = Array(width * width, { i -> EmptyCell((i / width) + 1, (i % width) + 1) })

    override fun getCellOrNull(i: Int, j: Int): Cell? =
            if (i in 1..width && j in 1..width) board[(i - 1) * width + (j - 1)] else null

    override fun getCell(i: Int, j: Int): Cell =
            getCellOrNull(i, j) ?: throw IllegalArgumentException("i,j have to be in 1..$width and 1..$width")

    override fun getAllCells(): Collection<Cell> =
            (1..width).flatMap { i -> (1..width).map { j -> board[(i - 1) * width + (j - 1)] } }

    override fun getRow(i: Int, jRange: IntProgression): List<Cell> =
            if (i in 1..width) jRange.filter { it in 1..width }.map { j -> board[(i - 1) * width + (j - 1)] }
            else emptyList()

    override fun getColumn(iRange: IntProgression, j: Int): List<Cell> =
            if (j in 1..width) iRange.filter { it in 1..width }.map { i -> board[(i - 1) * width + (j - 1)] }
            else emptyList()

    override fun Cell.getNeighbour(direction: Direction): Cell? =
            when (direction) {
                Direction.UP -> getCellOrNull(i - 1, j)
                Direction.DOWN -> getCellOrNull(i + 1, j)
                Direction.RIGHT -> getCellOrNull(i, j + 1)
                Direction.LEFT -> getCellOrNull(i, j - 1)
            }
}

class GameBoardImpl<T>(width: Int) : GameBoard<T>, SquareBoardImpl(width) {

    private class CellWithValue<T>(i: Int, j: Int) : EmptyCell(i, j) {
        var value: T? = null
    }

    override val board: Array<Cell> = Array(width * width, { i -> CellWithValue<T>((i / width) + 1, (i % width) + 1) })

    private fun Cell.fetch(): T? {
        return (this as CellWithValue<T>).value
    }

    private fun Cell.put(value: T?) {
        (this as CellWithValue<T>).value = value
    }

    override fun get(cell: Cell): T? {
        return getCellOrNull(cell.i, cell.j)?.fetch()
    }

    override fun set(cell: Cell, value: T?) {
        getCellOrNull(cell.i, cell.j)?.put(value)
    }

    override fun filter(predicate: (T?) -> Boolean): Collection<Cell> {
        return getAllCells().filter { cell -> predicate(cell.fetch()) }
    }

    override fun find(predicate: (T?) -> Boolean): Cell? {
        return getAllCells().firstOrNull { cell -> predicate(cell.fetch()) }
    }

    override fun any(predicate: (T?) -> Boolean): Boolean {
        return getAllCells().any { cell -> predicate(cell.fetch()) }
    }

    override fun all(predicate: (T?) -> Boolean): Boolean {
        return getAllCells().all { cell -> predicate(cell.fetch()) }
    }
}


fun createSquareBoard(width: Int): SquareBoard = SquareBoardImpl(width)

fun <T> createGameBoard(width: Int): GameBoard<T> = GameBoardImpl(width)

