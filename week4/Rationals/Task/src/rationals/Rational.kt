package rationals

import java.math.BigInteger

class Rational(n: BigInteger, d: BigInteger) : Comparable<Rational> {

    private val numerator: BigInteger
    private val denominator: BigInteger

    init {
        val nn = normalize(n, d)
        numerator = nn.first
        denominator = nn.second
    }

    override fun toString() = if (denominator == BigInteger.ONE) "$numerator" else "$numerator/$denominator"

    override operator fun compareTo(other: Rational): Int {
        val lhs = numerator * other.denominator
        val rhs = denominator * other.numerator
        if (lhs < rhs) return -1
        return if (lhs > rhs) 1 else 0
    }

    override fun equals(other: Any?): Boolean {
        if (other == null) return false
        if (other.javaClass != this.javaClass) return false
        val b = other as Rational
        return compareTo(b) == 0
    }

    override fun hashCode(): Int {
        return this.toString().hashCode()
    }

    // return a + b, staving off overflow
    operator fun plus(b: Rational): Rational {
        val a = this

        // special cases
        if (a == zero) return b
        if (b == zero) return a

        // Find gcd of numerators and denominators
        val f = gcd(a.numerator, b.numerator)
        val g = gcd(a.denominator, b.denominator)

        // add cross-product terms for numerator
        val s = Rational(a.numerator / f * (b.denominator / g) + b.numerator / f * (a.denominator / g),
                lcm(a.denominator, b.denominator))

        // multiply back in
        return s * f.toRational()
    }

    operator fun unaryMinus(): Rational {
        return Rational(-numerator, denominator)
    }

    operator fun minus(b: Rational): Rational {
        return this + (-b)
    }

    // return a * b, staving off overflow as much as possible by cross-cancellation
    operator fun times(b: Rational): Rational {
        val c = Rational(numerator, b.denominator)
        val d = Rational(b.numerator, denominator)
        return Rational(c.numerator * d.numerator, c.denominator * d.denominator)
    }

    private fun reciprocal(): Rational {
        return Rational(denominator, numerator)
    }

    operator fun div(b: Rational): Rational {
        return this * b.reciprocal()
    }

    companion object {
        val zero = 0.toRational()

        private fun normalize(numerator: BigInteger, denominator: BigInteger): Pair<BigInteger, BigInteger> {
            return when (denominator) {
                BigInteger.ZERO -> throw ArithmeticException("denominator is zero")
                BigInteger.ONE -> numerator to denominator
                else -> {
                    // reduce fraction
                    val g = gcd(numerator, denominator)
                    var num = numerator / g
                    var den = denominator / g

                    // needed only for negative numbers
                    if (den.compareTo(BigInteger.ZERO) < 0) {
                        den = -den
                        num = -num
                    }
                    return num to den
                }
            }
        }

        // return gcd(|m|, |n|)
        private fun gcd(m: BigInteger, n: BigInteger): BigInteger {
            var mm = m
            var nn = n
            if (mm < BigInteger.ZERO) mm = -mm
            if (nn < BigInteger.ZERO) nn = -nn
            return if (BigInteger.ZERO == nn)
                mm
            else
                gcd(nn, mm % nn)
        }

        // return lcm(|m|, |n|)
        private fun lcm(m: BigInteger, n: BigInteger): BigInteger {
            var mm = m
            var nn = n
            if (mm < BigInteger.ZERO) mm = -mm
            if (nn < BigInteger.ZERO) nn = -nn
            return mm * (nn / gcd(mm, nn))    // parentheses important to avoid overflow
        }
    }
}


infix fun BigInteger.divBy(d: BigInteger) = Rational(this, d)
infix fun Long.divBy(d: Long) = Rational(toBigInteger(), d.toBigInteger())
infix fun Int.divBy(d: Int) = Rational(toBigInteger(), d.toBigInteger())

fun BigInteger.toRational() = Rational(this, BigInteger.ONE)
fun Long.toRational() = toBigInteger().toRational()
fun Int.toRational() = toBigInteger().toRational()

fun String.toRational(): Rational {
    val nd = split("/")
    return when (nd.size) {
        1 -> nd[0].toBigInteger().toRational()
        2 -> Rational(nd[0].toBigInteger(), nd[1].toBigInteger())
        else -> throw Exception("no rational number detected")
    }
}


fun main(args: Array<String>) {
    val r1 = 1 divBy 2
    val r2 = 2000000000L divBy 4000000000L
    println(r1 == r2)

    println((2 divBy 1).toString() == "2")

    println((-2 divBy 4).toString() == "-1/2")
    println("117/1098".toRational().toString() == "13/122")

    println("1/2".toRational() - "1/3".toRational() == "1/6".toRational())
    println("1/2".toRational() + "1/3".toRational() == "5/6".toRational())

    println(-(1 divBy 2) == (-1 divBy 2))

    println((1 divBy 2) * (1 divBy 3) == "1/6".toRational())
    println((1 divBy 2) / (1 divBy 4) == "2".toRational())

    println((1 divBy 2) < (2 divBy 3))
    println((1 divBy 2) in (1 divBy 3)..(2 divBy 3))

    println("912016490186296920119201192141970416029".toBigInteger() divBy
            "1824032980372593840238402384283940832058".toBigInteger() == 1 divBy 2)
}