package games.gameOfFifteen

import board.Cell
import board.Direction
import board.createGameBoard
import games.game.Game

/*
 * Implement the Game of Fifteen (https://en.wikipedia.org/wiki/15_puzzle).
 * When you finish, you can play the game by executing 'PlayGameOfFifteen'
 * (or choosing the corresponding run configuration).
 */
fun newGameOfFifteen(initializer: GameOfFifteenInitializer = RandomGameInitializer()): Game =
        GameOfFifteen(initializer)

class GameOfFifteen(private val initializer: GameOfFifteenInitializer) : Game {

    private fun List<Int>.isSorted() = zipWithNext { s1, s2 -> s1 <= s2 }.all { it }

    private val board = createGameBoard<Int?>(4)

    override fun initialize() {
        board.getAllCells().forEachIndexed { index, cell -> board[cell] = initializer.initialPermutation.getOrNull(index) }
    }

    override fun canMove() = true

    override fun hasWon() = (1..board.width).all { it ->
        board.getRow(it, 1..board.width).map { cell -> board[cell] ?: 16 }.isSorted() &&
                board.getColumn(1..board.width, it).map { cell -> board[cell] ?: 16 }.isSorted()
    }

    override fun processMove(direction: Direction) {
        val emptyCell = emptyCell()
        emptyCell.getNeighbour(direction.reversed())?.run {
            swapValues(emptyCell, this)
        }
    }

    override fun get(i: Int, j: Int): Int? = board.run { get(getCell(i, j)) }

    private fun swapValues(from: Cell, to: Cell) {
        val value = board[to]
        board[to] = board[from]
        board[from] = value
    }

    private fun emptyCell(): Cell = board.getAllCells().filter { cell -> board[cell] == null }.first()

    private fun Cell.getNeighbour(direction: Direction): Cell? =
        when (direction) {
            Direction.UP -> board.getCellOrNull(i - 1, j)
            Direction.DOWN -> board.getCellOrNull(i + 1, j)
            Direction.RIGHT -> board.getCellOrNull(i, j + 1)
            Direction.LEFT -> board.getCellOrNull(i, j - 1)
        }
}

