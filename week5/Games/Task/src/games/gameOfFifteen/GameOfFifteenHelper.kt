package games.gameOfFifteen

/*
 * This function should return the parity of the permutation.
 * true - the permutation is even
 * false - the permutation is odd
 * https://en.wikipedia.org/wiki/Parity_of_a_permutation

 * If the game of fifteen is started with the wrong parity, you can't get the correct result
 *   (numbers sorted in the right order, empty cell at last).
 * Thus the initial permutation should be correct.
 */

fun isEven(permutation: List<Int>): Boolean {

    fun IntRange.pairs(): List<Pair<Int, Int>> = this.flatMap {
        a -> (a..this.last).map { b -> a to b }
    }

    fun Int.isEven() = this % 2 == 0

    return (0..permutation.lastIndex).pairs()
            .map { permutation[it.first] to permutation[it.second] }
            .count { it.first > it.second }
            .isEven()
}